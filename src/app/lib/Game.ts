import {Engine} from './Engine';
import {Panel} from './views/Panel';
import {Label} from './views/Label';
import {Button} from './views/Button';
import {Imagen} from './views/Imagen';
import {Window} from './views/Window';
import {EventManager} from './EventManager';
import {Telacomiste} from './views/Telacomiste';
import {EventClickListener, EventKeyPressListener, EventHoverListener} from './EventManagerListener';
import {EventClick, EventKeyPress, EventHover} from './Events';
import {Pregunta} from './Pregunta';
import {SinglePlayer} from './SinglePlayer';
import { MultiPlayer } from './Multiplayer';

export class Game implements EventClickListener, EventKeyPressListener, EventHoverListener {
    private engine: Engine;

    private enfasisColor: string;

    private arrPreguntas: Pregunta[];

    private singleplayer: SinglePlayer;
    private multiplayer: MultiPlayer;

    //MENU
    private titulo: Label;
    private btnSingle: Button;
    private btnMulti: Button;
    private btnHelp: Button;

    private numLoading: Label;

    //PREGUNTA
    private preg: Label;
    private btnRes1: Button;
    private btnRes2: Button;
    private btnRes3: Button;
    private btnRes4: Button;


    constructor(engine: Engine){
        this.engine = engine;
        this.arrPreguntas = new Array<Pregunta>();
    }

    private loadComponentes():void{
        this.enfasisColor = "#18AA8D";
        this.engine.body.bgColor = this.enfasisColor;
        this.menu();
    }

    public startGame():void{
        this.loadComponentes();
        this.engine.start();
    }

    public stopGame():void{
        this.engine.stop();
    }

    private menu(){
        this.titulo = new Label();
        this.engine.body.addChild(this.titulo);
        this.titulo.setSize(100, 40, true, true, "relative");
        this.titulo.setLocation(50, 15, true, true, "relative");
        this.titulo.text = "QuizKeo";
        this.titulo.fontSize = "100px";
        this.titulo.fontColor = "#FFF";
        this.titulo.textAling = "middle";
    
        this.btnSingle = new Button();
        this.engine.body.addChild(this.btnSingle);
        this.btnSingle.setSize(20, 8, true, true, "relative");
        this.btnSingle.setLocation(40, 45, true, true, "relative");
        this.btnSingle.bgColor = "#FFF";
        this.btnSingle.borderColor = this.enfasisColor;
        this.btnSingle.fontColor = this.enfasisColor;
        this.btnSingle.text = "Single Player";
        EventManager.ins.addClickListener(new EventClick(this, this.btnSingle, "btn-single"));

        this.btnMulti = new Button();
        this.engine.body.addChild(this.btnMulti);
        this.btnMulti.setSize(20, 8, true, true, "relative");
        this.btnMulti.setLocation(25, 65, true, true, "relative");
        this.btnMulti.bgColor = "#FFF";
        this.btnMulti.borderColor = this.enfasisColor;
        this.btnMulti.fontColor = this.enfasisColor;
        this.btnMulti.text = "Multi Player";
        EventManager.ins.addClickListener(new EventClick(this, this.btnMulti, "btn-multi"));

        this.btnHelp = new Button();
        this.engine.body.addChild(this.btnHelp);
        this.btnHelp.setSize(20, 8, true, true, "relative");
        this.btnHelp.setLocation(55, 65, true, true, "relative");
        this.btnHelp.bgColor = "#FFF";
        this.btnHelp.borderColor = this.enfasisColor;
        this.btnHelp.fontColor = this.enfasisColor;
        this.btnHelp.text = "Help";
        EventManager.ins.addClickListener(new EventClick(this, this.btnHelp, "btn-help"));
    }

    private closeMenu(){
        this.btnSingle.removeFromDOM();
        this.btnHelp.removeFromDOM();
        this.titulo.removeFromDOM();
        this.btnHelp.removeFromDOM();
        EventManager.ins.removeListenerClick("btn-single");
        EventManager.ins.removeListenerClick("btn-help");
    }

    onClick(id: string, e: MouseEvent){
        if(id == "btn-single"){
            this.closeMenu();
            this.singleplayer = new SinglePlayer(this.engine);
            this.singleplayer.start();
        }else if(id == "btn-multi"){
            this.closeMenu();
            this.multiplayer = new MultiPlayer(this.engine);
        }
    }

    onKeyPress(id: string, e: KeyboardEvent){
        
    }

    onHover(id: string, e: MouseEvent){
        
    }
}