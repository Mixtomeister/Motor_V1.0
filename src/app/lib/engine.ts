import {NgZone} from '@angular/core';
import {View} from './views/View';
import {Panel} from './views/Panel';

export class Engine{
    private context: CanvasRenderingContext2D;
    private ngZone: NgZone;
    private isOn: boolean;
    public body: Panel;

    constructor(context: CanvasRenderingContext2D, ngZone: NgZone){
        this.context = context;
        this.ngZone = ngZone;
        this.body = new Panel();
        this.body.setLocation(0, 0, false, false, "absolute");
        this.body.setSize(100, 100, true, true, "absolute");
        this.body.bgColor = "#FFF";
    }

    public start():void{
        this.isOn = true;
        requestAnimationFrame(() => this.loop());
    }

    public stop():void{
        this.isOn = false;
    }

    private loop():void{
        if(this.isOn){
            this.update(this.body);
            this.ngZone.runOutsideAngular(() => this.paint(this.body));
            requestAnimationFrame(() => this.loop());
        }else{
            this.update(this.body);
            this.ngZone.runOutsideAngular(() => this.paint(this.body));
        }
    }

    public paint(view: View):void {
        view.paint(this.context);
        for(var i in view.childs){
            if(view.childs[i].visible){
                this.paint(view.childs[i]);
            }
        }
    }

    public update(view: View):void{
        view.update();
        for(var i in view.childs){
            this.update(view.childs[i]);
        }
    }
}