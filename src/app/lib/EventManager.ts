import {EventResizeListener, EventClickListener, EventKeyPressListener, EventHoverListener} from './EventManagerListener';
import {EventClick, EventResize, EventKeyPress, EventHover} from './Events';


export class EventManager{
    public static ins: EventManager = new EventManager();
    public resizeListener: EventResize[];
    public clickListener: EventClick[];
    public keyPressListener: EventKeyPress[];
    public hoverListener: EventHover[];

    public constructor(){
        window.addEventListener("click", this.click);
        window.addEventListener("resize", this.resize);
        window.addEventListener("keypress", this.keypress);
        window.addEventListener("mousemove",  this.hover);
        this.resizeListener = new Array<EventResize>();
        this.clickListener = new Array<EventClick>();
        this.keyPressListener = new Array<EventKeyPress>();
        this.hoverListener = new Array<EventHover>();
    }

    public addResizeListener(ob: EventResize):void{
        this.resizeListener.push(ob);
    }

    public addClickListener(ob: EventClick):void{
        this.clickListener.push(ob);
    }

    public addKeyPressListener(ob: EventKeyPress):void{
        this.keyPressListener.push(ob);
    }

    public addHoverListener(ob: EventHover):void{
        this.hoverListener.push(ob);
    }

    private click(e: MouseEvent){
        for (let event of EventManager.ins.clickListener) {
            var flag = false;
            if((event.view.coordX < e.pageX) && (e.pageX < event.view.coordX + event.view.width) && (event.view.coordY < e.pageY) && (e.pageY < event.view.coordY + event.view.height) && event.view.visible){
                if(event.view.childs.length == 0){
                    flag = true;
                }else{
                    flag = true;
                    for(var a in event.view.childs){
                        if((event.view.childs[a].coordX < e.pageX) && (e.pageX < event.view.childs[a].coordX + event.view.childs[a].width) 
                        && (event.view.childs[a].coordY < e.pageY) && (e.pageY < event.view.childs[a].coordY + event.view.childs[a].height) && event.view.childs[a].visible){
                            flag = false;
                        }               
                    }
                }
            }
            if(flag){
                event.listener.onClick(event.id, e);
            }
        }
    }

    private resize():void{
        for (let event of EventManager.ins.resizeListener) {
            event.listener.onResize(event.id);
        }
    }

    private keypress(e: KeyboardEvent){
        for (let event of EventManager.ins.keyPressListener) {
            if(e.charCode == event.code){
                event.listener.onKeyPress(event.id, e);
            }
        }
    }

    private hover(e: MouseEvent){
        for (let event of EventManager.ins.hoverListener) {
            var flag = false;
            if((event.view.coordX < e.pageX) && (e.pageX < event.view.coordX + event.view.width) && (event.view.coordY < e.pageY) && (e.pageY < event.view.coordY + event.view.height) && event.view.visible){
                if(event.view.childs.length == 0){
                    flag = true;
                }else{
                    flag = true;
                    for(var a in event.view.childs){
                        if((event.view.childs[a].coordX < e.pageX) && (e.pageX < event.view.childs[a].coordX + event.view.childs[a].width) 
                        && (event.view.childs[a].coordY < e.pageY) && (e.pageY < event.view.childs[a].coordY + event.view.childs[a].height) && event.view.childs[a].visible){
                            flag = false;
                        }               
                    }
                }
            }
            if(flag){
                event.listener.onHover(event.id, e);
            }
        }
    }

    public removeListenerClick(id: string){
        for(var i = 0; i < this.clickListener.length; i++){
            if(this.clickListener[i].id == id){
                this.clickListener.splice(i, 1);
            }
        }
    }

    public removeListenerResize(id: string){
        for(var i = 0; i < this.resizeListener.length; i++){
            if(this.resizeListener[i].id == id){
                this.resizeListener.splice(i, 1);
            }
        }
    }

    public removeListenerKeyPress(id: string){
        for(var i = 0; i < this.keyPressListener.length; i++){
            if(this.keyPressListener[i].id == id){
                this.keyPressListener.splice(i, 1);
            }
        }
    }

    public removeListenerHover(id: string){
        for(var i = 0; i < this.hoverListener.length; i++){
            if(this.hoverListener[i].id == id){
                this.hoverListener.splice(i, 1);
            }
        }
    }
}