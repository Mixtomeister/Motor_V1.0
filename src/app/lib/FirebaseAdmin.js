
export class FirebaseAdmin{
    constructor(listener){
        this.listener = listener;
        firebase.initializeApp({
            apiKey: "AIzaSyBVFt-yvdSY75XHeu7H176HUeudjLtF5NU",
            authDomain: "quizkeo-magodeoz.firebaseapp.com",
            databaseURL: "https://quizkeo-magodeoz.firebaseio.com",
            projectId: "quizkeo-magodeoz",
            storageBucket: "quizkeo-magodeoz.appspot.com",
            messagingSenderId: "508410030031"
        });
        this.database = firebase.database();
    }

    startListenPlayer(){
        this.database.ref('match/player').on('value', (snapshot) => {
            this.listener.onPlayerChange(snapshot.val())
        })
    }

    startListenTurn(player){
        this.database.ref('match/' + player).on('value', (snapshot) => {
            this.listener.onTurnChange(snapshot.val())
        })
    }

    switchTurn(p1){
        if(p1){
            this.database.ref('match/p1').set(0);
            this.database.ref('match/p2').set(1);
        }else{
            this.database.ref('match/p1').set(1);
            this.database.ref('match/p2').set(0);
        }
    }

    setPlayer(num){
        this.database.ref('match/player').set(num);
    }
}