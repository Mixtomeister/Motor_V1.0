import {View} from './View';
import {Panel} from './Panel';
import {Label} from './Label';
import {Button} from './Button';
import {EventManager} from '../EventManager';

export class Window extends View {

    private backpanel: Panel;
    private titleBar: Panel;
    private lbltitle: Label;
    private btnClose: Button;

    titleBarColor: string;
    title:string;

    public constructor(){
        super();
        this.bgColor = "#FFF";
        this.backpanel = new Panel();
        this.titleBar = new Panel();
        this.lbltitle = new Label();
        this.btnClose = new Button();
        this.title = "";
        this.btnClose.bgColor = "#FF0000";
        this.btnClose.borderColor = "#FF0000";
        this.btnClose.text = "X";
        this.btnClose.fontColor = "#FFF";
        this.titleBarColor = "#00BFFF";
    }

    paint(context: CanvasRenderingContext2D){
        this.backpanel.paint(context);
        this.titleBar.paint(context);
        this.lbltitle.paint(context);
        this.btnClose.paint(context);
    }

    update(){
        this.updateLocation();
        this.updateSize();
        this.backpanel.parent = this.parent;
        this.titleBar.parent = this.parent;
        this.lbltitle.parent = this.parent;
        this.btnClose.parent = this.parent;
        this.backpanel.coordX = this.coordX;
        this.backpanel.coordY = this.coordY;
        this.backpanel.width = this.width;
        this.backpanel.height = this.height;
        this.titleBar.coordX = this.coordX;
        this.titleBar.coordY = this.coordY;
        this.titleBar.width = this.width;
        this.titleBar.height = 30;
        this.btnClose.coordX = (this.coordX + this.width) - 50;
        this.btnClose.coordY = this.coordY;
        this.btnClose.width = 50;
        this.btnClose.height = 30;
        this.lbltitle.coordX = this.coordX + 15;
        this.lbltitle.coordY = this.coordY + 5;
        this.lbltitle.width = this.width;
        this.lbltitle.height = 30;

        this.backpanel.bgColor = this.bgColor;
        this.titleBar.bgColor = this.titleBarColor;
        this.lbltitle.text = this.title;
        this.backpanel.update();
        this.titleBar.update();
        this.lbltitle.update();
        this.btnClose.update();
    }

    public close(){
        this.removeFromDOM();
    }

    public getBtnClose():Button{
        return this.btnClose;
    }
}