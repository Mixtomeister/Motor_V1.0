export interface EventResizeListener{
    onResize(id: string);
}
export interface EventClickListener{
    onClick(id: string, e: MouseEvent);
}
export interface EventKeyPressListener{
    onKeyPress(id: string, e: KeyboardEvent);
}
export interface EventHoverListener{
    onHover(id: string, e: MouseEvent);
}