import {Engine} from './Engine';
import {Panel} from './views/Panel';
import {Label} from './views/Label';
import {Button} from './views/Button';
import {Imagen} from './views/Imagen';
import {Window} from './views/Window';
import {EventManager} from './EventManager';
import {Telacomiste} from './views/Telacomiste';
import {EventClickListener, EventKeyPressListener, EventHoverListener} from './EventManagerListener';
import {EventClick, EventKeyPress, EventHover} from './Events';
import {Pregunta} from './Pregunta';
import {FirebaseAdmin} from './FirebaseAdmin';
import {FirebaseAdminListener} from './FirebaseAdminListener';

export class MultiPlayer implements EventClickListener, FirebaseAdminListener{

    private preguntas: Pregunta[];
    private actPre: number;
    private engine: Engine;
    private firebaseAdmin: FirebaseAdmin;
    private end: boolean;
    private started: boolean;
    private p1: boolean;
    private turn: number;
    private aciertos: number;
    private fallos: number;

    private numLoading: Label;


    //PREGUNTA
    private preg: Label;
    private btnRes1: Button;
    private btnRes2: Button;
    private btnRes3: Button;
    private btnRes4: Button;

    constructor(engine: Engine){
        this.preguntas = new Array<Pregunta>();
        this.engine = engine;
        this.aciertos = 0;
        this.fallos = 0;
        this.end = false;
        this.started = false;
        this.p1 = false;
        this.cargarPreguntas();
        this.actPre = Math.round(Math.random() * ((this.preguntas.length - 1) - 0) + 0);
        this.waitPlayer();
        this.firebaseAdmin = new FirebaseAdmin(this);
        this.firebaseAdmin.startListenPlayer();
    }

    private cargarPreguntas(){
        this.preguntas.push(new Pregunta("She ____ (think) Manuel is crazy.", ["think", "does think", "thinks", "all are correct", "thinks"]));
        this.preguntas.push(new Pregunta("They ____ (not know) what to say.", ["don't know", "not know", "doesn't know", "don't knows", "don't know"]));
        this.preguntas.push(new Pregunta("____ (feel/she) ok?", ["She does feel", "She feels", "Does she feel", "She do feels", "Does she feel"]));
        this.preguntas.push(new Pregunta("He ____ (not be) a relative of mine.", ["doesn't be", "isn't", "aren't", "None is correct", "isn't"]));
        this.preguntas.push(new Pregunta("She ____ (wash) her car every week.", ["washes", "washs", "wash", "washies", "washes"]));
        this.preguntas.push(new Pregunta("Paul ____ (sleep) seven hours a day.", ["sleepies", "sleep", "sleeps", "sleepes", "sleeps"]));
        this.preguntas.push(new Pregunta("Mary and John ____ (be) my cousins.", ["be", "is", "None is correct", "are", "are"]));
        this.preguntas.push(new Pregunta("She always ____ (win).", ["win", "winies", "wines", "wins", "wins"]));
        this.preguntas.push(new Pregunta("James ____ (cry) very easily.", ["cries", "does cry", "crys", "cryes", "cries"]));
        this.preguntas.push(new Pregunta("She ____ (pray) in church every Sunday.", ["pray", "prayes", "prays", "praies", "prays"]));
        this.preguntas.push(new Pregunta("Sarah ____ (not like) k-pop music.", ["doesn't like", "doesn't likes", "don't likes", "don't like", "doesn't like"]));
        this.preguntas.push(new Pregunta("____ (play) football everyday?", ["Jhon does play", "Do Jhon play", "Does Jhon plays", "Does Jhon play", "Does Jhon play"]));
        this.preguntas.push(new Pregunta("____ (be) she a friend of yours?", ["Be", "Is", "Are", "Am", "Is"]));
        this.preguntas.push(new Pregunta("____ (be) they in love?", ["Are", "Do", "Be", "Is", "Are"]));
        this.preguntas.push(new Pregunta("____ (believe/ Mary) in God?", ["Does Mary believes", "Does Mary believe", "Do Mary believe", "Mary does believe", "Does Mary believe"]));
        this.preguntas.push(new Pregunta("Who ____ (be) your favourite football player? ", ["is", "am", "are", "be", "is"]));

        this.preguntas.push(new Pregunta("George ____ (go) to school everyday.", ["doesn't goes", "go", "don't goes", "goes", "goes"]));
        this.preguntas.push(new Pregunta("Mary ____ (not Know) who you are.", ["doesn't knows", "don't know", "doesn't know", "not knows", "doesn't know"]));
        this.preguntas.push(new Pregunta("She ____ (be) my best friend.", ["not are", "is", "not is", "are", "is"]));
        this.preguntas.push(new Pregunta("____ (work) as teachers?", ["Do they work", "They do work", "They work", "They works", "Do they work"]));

        this.preguntas.push(new Pregunta("John ____ (watch) TV twice a day", ["watchs", "watch", "watchies", "watches", "watches"]));
        this.preguntas.push(new Pregunta("Michael ____ (not like) mobile phones.", ["does likes", "doesn't likes", "doesn't like", "do likes", "doesn't like"]));
        this.preguntas.push(new Pregunta("____ (know) the truth?", ["Do Mary knows", "Does Mary know", "Does Mary knows", "Is Mary know", "Does Mary know"]));
        this.preguntas.push(new Pregunta("He ____ (work) as a shop - assistant.", ["works", "work", "workes", "does works", "works"]));
        this.preguntas.push(new Pregunta("The baby ____ (cry) everyday.", ["cry", "cries", "crys", "doesn't cries", "cries"]));
        this.preguntas.push(new Pregunta("Women ____ (be) more intelligent than men.", ["isn't", "are", "is", "be", "are"]));
    }

    public waitPlayer(){
        this.numLoading = new Label();
        this.engine.body.addChild(this.numLoading);
        this.numLoading.setSize(100, 40, true, true, "relative");
        this.numLoading.setLocation(50, 40, true, true, "relative");
        this.numLoading.text = "3";
        this.numLoading.fontSize = "125px";
        this.numLoading.fontColor = "#FFF";
        this.numLoading.textAling = "middle";
        this.engine.body.bgColor = "#18AA44";
        this.numLoading.text = "Waiting for another player...";
        this.engine.body.bgColor = "#187eaa";
    }

    public start(){
        setTimeout(() => {
            this.finish();
        }, 30000);
        this.numLoading.text = "3";
        this.engine.body.bgColor = "#18AA44";
        setTimeout(() => {
            this.numLoading.text = "2";
            this.engine.body.bgColor = "#aa187e";
            setTimeout(() => {
                this.numLoading.text = "1";
                this.engine.body.bgColor = "#187eaa";
                setTimeout(() => {
                    this.engine.body.bgColor = "#DF3A01";
                    this.numLoading.removeFromDOM();
                    this.started = true;
                    this.wait();
                }, 1000);
            },1000);
        }, 1000);
    }

    public wait(){
        if(this.turn == 0){
            this.numLoading.text = "Wait your turn...";
            this.engine.body.addChild(this.numLoading);
            this.engine.body.bgColor = "#187eaa";
        }else{
            this.engine.body.bgColor = "#DF3A01";
            this.numLoading.removeFromDOM();
            this.pregunta();
        }
    }

    public pregunta(){
        this.preg = new Label();
        this.engine.body.addChild(this.preg);
        this.preg.setSize(100, 40, true, true, "relative");
        this.preg.setLocation(50, 10, true, true, "relative");
        this.preg.text = this.preguntas[this.actPre].title;
        this.preg.fontSize = "40px";
        this.preg.fontColor = "#FFF";
        this.preg.textAling = "middle";

        this.btnRes1 = new Button();
        this.engine.body.addChild(this.btnRes1);
        this.btnRes1.setSize(50, 35, true, true, "relative");
        this.btnRes1.setLocation(0, 30, true, true, "relative");
        this.btnRes1.bgColor = "#FFF";
        this.btnRes1.borderColor = "#18AA8D";
        this.btnRes1.fontColor = "#18AA8D";
        this.btnRes1.text = this.preguntas[this.actPre].r1;
        this.btnRes1.fontSize = "40px";
        EventManager.ins.addClickListener(new EventClick(this, this.btnRes1, "btn-res1"));

        this.btnRes2 = new Button();
        this.engine.body.addChild(this.btnRes2);
        this.btnRes2.setSize(50, 35, true, true, "relative");
        this.btnRes2.setLocation(50, 30, true, true, "relative");
        this.btnRes2.bgColor = "#FFF";
        this.btnRes2.borderColor = "#18AA8D";
        this.btnRes2.fontColor = "#18AA8D";
        this.btnRes2.text = this.preguntas[this.actPre].r2;
        this.btnRes2.fontSize = "40px";
        EventManager.ins.addClickListener(new EventClick(this, this.btnRes2, "btn-res2"));

        this.btnRes3 = new Button();
        this.engine.body.addChild(this.btnRes3);
        this.btnRes3.setSize(50, 35, true, true, "relative");
        this.btnRes3.setLocation(0, 65, true, true, "relative");
        this.btnRes3.bgColor = "#FFF";
        this.btnRes3.borderColor = "#18AA8D";
        this.btnRes3.fontColor = "#18AA8D";
        this.btnRes3.text = this.preguntas[this.actPre].r3;
        this.btnRes3.fontSize = "40px";
        EventManager.ins.addClickListener(new EventClick(this, this.btnRes3, "btn-res3"));

        this.btnRes4 = new Button();
        this.engine.body.addChild(this.btnRes4);
        this.btnRes4.setSize(50, 35, true, true, "relative");
        this.btnRes4.setLocation(50, 65, true, true, "relative");
        this.btnRes4.bgColor = "#FFF";
        this.btnRes4.borderColor = "#18AA8D";
        this.btnRes4.fontColor = "#18AA8D";
        this.btnRes4.text = this.preguntas[this.actPre].r4;
        this.btnRes4.fontSize = "40px";
        EventManager.ins.addClickListener(new EventClick(this, this.btnRes4, "btn-res4"));
    }

    private closePregunta(){
        this.preg.removeFromDOM();
        this.btnRes1.removeFromDOM();
        this.btnRes2.removeFromDOM();
        this.btnRes3.removeFromDOM();
        this.btnRes4.removeFromDOM();
        EventManager.ins.removeListenerClick("btn-res1");
        EventManager.ins.removeListenerClick("btn-res2");
        EventManager.ins.removeListenerClick("btn-res3");
        EventManager.ins.removeListenerClick("btn-res4");
    }

    private good(){
        this.actPre = Math.round(Math.random() * ((this.preguntas.length - 1) - 0) + 0);
        this.aciertos++;
        this.engine.body.bgColor = "#01DF01";
        this.engine.body.addChild(this.numLoading);
        this.numLoading.text = "Great!";
        setTimeout(() => {
            this.engine.body.bgColor = "#DF3A01";
            this.numLoading.removeFromDOM();
            this.firebaseAdmin.switchTurn(this.p1);
        }, 500);
    }

    private fail(){
        this.actPre = Math.round(Math.random() * ((this.preguntas.length - 1) - 0) + 0);
        this.fallos++;
        this.engine.body.bgColor = "#DF0101";
        this.engine.body.addChild(this.numLoading);
        this.numLoading.text = "Fail...";
        setTimeout(() => {
            this.engine.body.bgColor = "#DF3A01";
            this.numLoading.removeFromDOM();
            if(!this.end){
                this.pregunta();
            }
        }, 500);
    }

    private finish(){
        this.end = true;
        this.closePregunta();
        this.engine.body.bgColor = "#187eaa";
        this.engine.body.addChild(this.numLoading);
        this.numLoading.text = "Timeout!";
        setTimeout(() => {
            this.numLoading.removeFromDOM();
            this.stats();
        }, 1000);
    }

    private stats(){
        this.engine.body.bgColor = "#187eaa";
        this.engine.body.addChild(this.numLoading);
        this.numLoading.text = "Correct answers: " + this.aciertos + "/" + (this.aciertos + this.fallos);
    }

    onClick(id: string, e: MouseEvent){
        if(id == "btn-res1"){
            this.closePregunta();
            if(this.preguntas[this.actPre].r1 == this.preguntas[this.actPre].rOK){
                this.good();
            }else{
                this.fail();
            }
        }else if(id == "btn-res2"){
            this.closePregunta();
            if(this.preguntas[this.actPre].r2 == this.preguntas[this.actPre].rOK){
                this.good();
            }else{
                this.fail();
            }
        }
        else if(id == "btn-res3"){
            this.closePregunta();
            if(this.preguntas[this.actPre].r3 == this.preguntas[this.actPre].rOK){
                this.good();
            }else{
                this.fail();
            }
        }
        else if(id == "btn-res4"){
            this.closePregunta();
            if(this.preguntas[this.actPre].r4 == this.preguntas[this.actPre].rOK){
                this.good();
            }else{
                this.fail();
            }
        }
    }

    onPlayerChange(player:number){
        console.log("PLAYER CHANGE: " + player);
        if(player == 0){
            this.p1 = true;
            this.firebaseAdmin.setPlayer(1);
        }else if(!this.p1 && player == 1){
            this.firebaseAdmin.setPlayer(2);
        }else if(player == 2){
            if(this.p1){
                this.firebaseAdmin.startListenTurn('p1');
            }else{
                this.firebaseAdmin.startListenTurn('p2');
            }
            this.start();
        }
    }

    onTurnChange(turn: number){
        this.turn = turn;
        if(this.started){
            this.wait();
        }
    }
}