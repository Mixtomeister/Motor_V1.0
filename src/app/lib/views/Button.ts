import {View} from './View';
import {Panel} from './Panel';
import {Label} from './Label';

export class Button extends View {

    text: string;
    borderColor: string;
    borderWidth: number;
    private borderPanel;
    private backPanel;

    constructor(){
        super();
        this.text = "";
        this.borderPanel = new Panel();
        this.backPanel = new Panel();
        this.text = "";
        this.borderWidth = 2;
    }

    paint(context: CanvasRenderingContext2D):void{
        this.borderPanel.paint(context);
        this.backPanel.paint(context);
        context.font = this.fontSize + " " + this.fontFamily;
        context.fillStyle = this.fontColor;
        context.textAlign = "center";
        context.textBaseline = "middle";
        context.fillText(this.text, this.coordX + (this.width / 2), this.coordY + (this.height / 2));
    }

    update():void{
        this.updateLocation();
        this.updateSize();
        this.borderPanel.parent = this.parent;
        this.backPanel.parent = this.parent;
        this.borderPanel.bgColor = this.borderColor;
        this.backPanel.bgColor = this.bgColor;
        this.borderPanel.coordX = this.coordX;
        this.borderPanel.coordY = this.coordY;
        this.borderPanel.width = this.width;
        this.borderPanel.height = this.height;
        this.backPanel.coordX = this.coordX + this.borderWidth;
        this.backPanel.coordY = this.coordY + this.borderWidth;
        this.backPanel.width = this.width - (this.borderWidth * 2) ;
        this.backPanel.height = this.height - (this.borderWidth * 2);
        this.borderPanel.update();
        this.backPanel.update();
    }
}