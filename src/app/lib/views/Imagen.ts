import {View} from './View';

export class Imagen extends View {

    ruta: string;
    img: HTMLImageElement;
    isLoad: boolean;
    
    public constructor(ruta: string){
        super();
        this.ruta = ruta;
        this.img = new Image();
        this.img.src = ruta;
        this.img.onload = (() => this.imgloaded(this));
    }

    private imgloaded(img: Imagen){
        this.isLoad = true;
    }

    paint(context: CanvasRenderingContext2D){
        if(this.isLoad){
            context.drawImage(this.img, this.coordX, this.coordY, this.width, this.height);
        }
    }

    update(){
        this.updateLocation();
        this.updateSize();
        this.img.src = this.ruta;
    }
}