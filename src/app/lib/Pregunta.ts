export class Pregunta{
    title: string;
    r1: string;
    r2: string;
    r3: string;
    r4: string;
    rOK: string;

    public constructor(title: string, r: string[]){
        this.title = title;
        this.r1 = r[0];
        this.r2 = r[1];
        this.r3 = r[2];
        this.r4 = r[3];
        this.rOK = r[4];
    }
}