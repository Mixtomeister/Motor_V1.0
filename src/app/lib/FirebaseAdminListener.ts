
export interface FirebaseAdminListener{
    onPlayerChange(player:number);
    onTurnChange(turn: number);
}