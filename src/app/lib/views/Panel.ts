import {View} from './View';
import {Imagen} from './Imagen';

export class Panel extends View {

    isbgImg: boolean;
    bgImg: Imagen;
    
    constructor(){
        super();
        this.w = 0;
        this.h = 0;
        this.isbgImg = false;
    }

    paint(context: CanvasRenderingContext2D):void{
        if(this.isbgImg){
            this.bgImg.paint(context);
        }else{
            context.fillStyle = this.bgColor;
            context.fillRect(this.coordX, this.coordY, this.width, this.height);
        }
    }

    update(){
        this.updateLocation();
        this.updateSize();
        if(this.isbgImg){
            this.bgImg.coordX = this.coordX;
            this.bgImg.coordY = this.coordY;
            this.bgImg.width = this.width;
            this.bgImg.height = this.height;
        }
    }

    public setBgImg(ruta: string){
        this.isbgImg = true;
        this.bgImg = new Imagen(ruta);
    }
}