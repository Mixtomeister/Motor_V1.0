import {View} from './View';
import {Panel} from './Panel';

export class Label extends View {

    private backPanel: Panel;

    text: string;
    textBaseline: string;

    constructor(){
        super();
        this.backPanel = new Panel();
        this.backPanel.parent = this.parent;
        this.text = "";
        this.textBaseline = "top";
    }

    paint(context: CanvasRenderingContext2D):void{
        
        this.backPanel.paint(context);
        context.font = this.fontSize + " " + this.fontFamily;
        context.fillStyle = this.fontColor;
        context.textAlign = this.textAling;
        context.textBaseline = this.textBaseline;
        context.fillText(this.text, this.coordX, this.coordY);
    }

    update(){
        this.updateLocation();
        this.updateSize();
        this.backPanel.parent = this.parent;
        this.backPanel.bgColor = this.bgColor;
        this.backPanel.setSize(this.w, this.h, this.wRelative, this.hRelative, this.sPosition);
        this.backPanel.setLocation(this.x, this.y, this.xRelative, this.yRelative, this.position);
        this.backPanel.update();
    }
}