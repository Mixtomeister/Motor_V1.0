import {EventResizeListener, EventClickListener, EventKeyPressListener, EventHoverListener} from './EventManagerListener';
import {View} from './views/View';

export class EventResize{
    id: string;
    listener: EventResizeListener;

    public constructor(listener: EventResizeListener, id: string){
        this.listener = listener;
        this.id = id;
    }
}

export class EventClick{
    id: string;
    listener: EventClickListener;
    view: View;

    public constructor(listener: EventClickListener, view: View, id: string){
        this.listener = listener;
        this.id = id;
        this.view = view;
    }
}

export class EventKeyPress{
    id: string;
    listener: EventKeyPressListener;
    code: number

    public constructor(listener: EventKeyPressListener, code:number, id: string){
        this.listener = listener;
        this.id = id;
        this.code = code;
    }
}

export class EventHover{
    id: string;
    listener: EventHoverListener;
    view: View;

    public constructor(listener: EventHoverListener, view: View, id: string){
        this.listener = listener;
        this.id = id;
        this.view = view;
    }
}