import {View} from './View';
import {Imagen} from './Imagen';

export class Telacomiste extends View{

    path: string[];
    limitImages: number;
    iWidth: number;
    iHeight: number;
    private imagenes: Imagen[];

    public constructor(w: number, h: number, limit: number){
        super();
        this.limitImages = limit;
        this.iWidth = w;
        this.iHeight = h;
        this.imagenes = new Array<Imagen>();
        this.path = new Array<string>();
    }

    paint(context: CanvasRenderingContext2D){
        for(var i = 0; i < this.imagenes.length; i++){
            this.imagenes[i].paint(context);
        }
    }

    update(){
        var img = new Imagen(this.path[Math.round(Math.random() * (this.path.length))]);
        img.coordX = Math.round(Math.random() * (document.body.clientWidth));
        img.coordY = Math.round(Math.random() * (document.body.clientHeight));
        img.width = this.iWidth;
        img.height = this.iHeight;
        this.imagenes.push(img);
        if(this.imagenes.length == this.limitImages){
            this.imagenes.splice(0, 1);
        }
    }
}