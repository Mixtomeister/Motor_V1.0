export class View {
    x:number;
    coordX: number;
    y:number;
    coordY: number;
    w:number;
    width: number;
    h:number;
    height: number;
    uid:string;
    childs:View[];
    parent: View;
    bgColor:string;
    visible:boolean;
    textAling: string;
    fontFamily: string;
    fontColor: string;
    fontSize: string;
    position: string;
    sPosition: string;
    wRelative: boolean;
    hRelative: boolean;
    xRelative: boolean;
    yRelative: boolean;

    constructor(){
        this.childs = Array<View>();
        this.uid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c){
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        })
        this.x = 0;
        this.y = 0;
        this.bgColor = "rgba(255, 255, 255, 0)";
        this.visible = true;
        this.textAling = "left";
        this.fontFamily = "Arial";
        this.fontSize = "15px";
        this.fontColor = "#000";
    }

    public addChild(view: View):void{
        view.parent = this;
        if(this.childs.indexOf(view) == -1){
            this.childs.push(view);
        }
    }

    public setLocation(x: number, y: number, xRelative: boolean, yRelative: boolean, lPosition: string){
        this.x = x;
        this.y = y;
        this.xRelative = xRelative;
        this.yRelative = yRelative;
        this.position = lPosition;
        this.updateLocation();
    }

    public setSize(w: number, h: number, wRelative: boolean, hRelative: boolean, sPosition: string){
        this.w = w;
        this.h = h;
        this.wRelative = wRelative;
        this.hRelative = hRelative;
        this.sPosition = sPosition;
        this.updateSize();
    }

    public removeFromDOM(){
        this.parent.childs.splice(this.parent.childs.indexOf(this), 1);
    }

    public updateLocation(){
        if(this.position == "relative"){
            if(this.xRelative){
                this.coordX = ((this.parent.width * this.x) / 100) + this.parent.coordX;
            }else{
                if(this.parent.wRelative){
                    this.coordX = (this.parent.coordX + this.x);
                }else{
                    this.coordX = (this.parent.x + this.x);
                }   
            }
            if(this.yRelative){
                this.coordY = ((this.parent.height * this.y) / 100) + this.parent.coordY;
            }else{
                if(this.parent.hRelative){
                    this.coordY = (this.parent.coordY + this.y);
                }else{
                    this.coordY = (this.parent.y + this.y);
                }
            }
        }else if(this.position == "absolute"){
            if(this.xRelative){
                this.coordX = (document.body.clientWidth * this.x) / 100;
            }else{
                this.coordX = this.x;   
            }
            if(this.yRelative){
                this.coordY = (document.body.clientHeight * this.y) / 100;
            }else{
                this.coordY = this.y;
            }
        }
    }
    
    public updateSize(){
        if(this.sPosition == "relative"){
            if(this.wRelative){
                this.width = (this.parent.width * this.w) / 100;
            }else{
                this.width = this.w;
            }
            if(this.hRelative){
                this.height = (this.parent.height * this.h) / 100;
            }else{
                this.height = this.h;
            }
        }else if(this.sPosition == "absolute"){
            if(this.wRelative){
                this.width = (document.body.clientWidth * this.w) / 100;
            }else{
                this.width = this.w;
            }
            if(this.hRelative){
                this.height = (document.body.clientHeight * this.h) / 100;
            }else{
                this.height = this.h;
            }
        }
    }

    paint(contexto: CanvasRenderingContext2D):void{}
    update():void{}
}