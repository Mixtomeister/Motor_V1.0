import {Component, OnInit, ViewChild, ElementRef, NgZone} from '@angular/core';
import {Engine} from '../lib/Engine';
import {Game} from '../lib/Game';
import {EventManager} from '../lib/EventManager';
import {EventResizeListener} from '../lib/EventManagerListener';
import {EventResize} from '../lib/Events';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})

export class CanvasComponent implements OnInit, EventResizeListener {
  @ViewChild('screen') screen: ElementRef;
  private context: CanvasRenderingContext2D;
  private engine: Engine;
  private game: Game;

  constructor(private ngZone: NgZone) {}

  ngOnInit() {
    EventManager.ins.addResizeListener(new EventResize(this, "canvas"));
    document.getElementById("screen").setAttribute("width", document.body.clientWidth.toString());
    document.getElementById("screen").setAttribute("height", document.body.clientHeight.toString());
    this.context = this.screen.nativeElement.getContext('2d');
    this.engine = new Engine(this.context, this.ngZone);
    this.game = new Game(this.engine);
    this.game.startGame();
  }

  public onResize(id: string){
    document.getElementById("screen").setAttribute("width", document.body.clientWidth.toString());
    document.getElementById("screen").setAttribute("height", document.body.clientHeight.toString());
  }
}